class CreateProductAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :product_areas do |t|
      t.references :product, foreign_key: true
      t.references :area, foreign_key: true
     	t.integer :status, default: 1

      t.timestamps  null: false
    end
  end
end
