class CreateCustomerAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_addresses do |t|
      t.references :customer, foreign_key: true
      t.string :name
      t.string :address1, null: false
      t.string :address2
      t.string :street, null: false
      t.string :city, null: false
      t.string :landmark, null: false
      t.string :pincode, null: false
      t.string :phone, null: false

      t.timestamps null: false
    end
  end
end
