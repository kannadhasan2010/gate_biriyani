class CreateOrderProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :order_products do |t|
      t.references :order, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :quantity, null: false, default: 1
      t.integer :amount, null: false
      t.text :description

      t.timestamps
    end
  end
end
