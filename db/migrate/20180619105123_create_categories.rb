class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.integer :status, default: 1

      t.timestamps null: false
    end
  end
end
