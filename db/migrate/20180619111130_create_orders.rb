class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :customer, foreign_key: true
      t.references :customer_address, foreign_key: true
      
      t.integer :status, default: 0
      

      t.timestamps  null: false
    end
  end
end
