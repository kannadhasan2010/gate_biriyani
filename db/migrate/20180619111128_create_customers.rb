class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email
      t.string :password
      t.string :phone
      t.string :email_verification_token
      t.string :phone_verification_token
      t.boolean :email_verified, default: false
      t.boolean :phone_verified, default: false
      t.string :salt
      t.string :auth_token
      t.string :image
      t.integer :status, default: 1
      t.string :reset_token
      t.datetime :reset_token_expiry
      t.integer :verification_pending_for, default: 0

      t.timestamps  null: false
    end
    add_index :customers, [:email, :phone], unique: true

  end
end
