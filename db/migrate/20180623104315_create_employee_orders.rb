class CreateEmployeeOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :employee_orders do |t|
      t.references :admin_user, foreign_key: true
      t.references :order, foreign_key: true
      t.string :description
      t.integer :status, default: 0

      t.timestamps  null: false
    end
  end
end
