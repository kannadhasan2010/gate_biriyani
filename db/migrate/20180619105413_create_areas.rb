class CreateAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :areas do |t|
      t.string :name
      t.string :code
      t.integer :status, default: 1

      t.timestamps null: false
    end
  end
end
