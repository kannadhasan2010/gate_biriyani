class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.references :category, foreign_key: true
      t.string :name
      t.text :description
      t.string :image1
      t.string :image2
      t.integer :price
      t.integer :status, default: 1

      t.timestamps  null: false
    end
  end
end
