require "bcrypt"

class EmailVerificationTokenGenerator

  include BCrypt
  attr_accessor :token
  
  def initialize(email)
    @email = email
    @token = nil
  end

  def generate_new_salt
    BCrypt::Engine.generate_salt
  end

  def generate_token
    @token = BCrypt::Engine.hash_secret(@email, generate_new_salt)
  end
end