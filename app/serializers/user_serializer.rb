class UserSerializer < ActiveModel::Serializer
  attributes :id, :user_name, :email, :phone, :email_verification_token, :phone_verification_token,  :email_verified, :phone_verified, :auth_token, :image, :status, :verification_pending_for
 end

