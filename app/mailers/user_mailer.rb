class UserMailer < ApplicationMailer

	default from: 'notifications@example.com'
 
  def welcome_email(user)
  	@user = user
  	@url = "https://gatebiriyani.herokuapp.com/guest/user/verify_email?email=#{user.email}&token=#{user.email_verification_token}"
  	email = user.email
    mail(to: email, subject: 'Welcome to Gate Biriyani')
  end

end
