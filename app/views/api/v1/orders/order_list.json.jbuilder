json.status true
json.code 200
json.orders do
	json.array! @orders do |order|
		json.id 						order.id
		json.customer_id 		order.customer_id
		json.customer_address order.customer_address
		json.status 				order.status
		json.products 	do		
			json.array! order.order_products do |order_product|
				json.product order_product.product
				json.quantity order_product.quantity
				json.amount order_product.amount
				json.description order_product.description
			end
		end
	end
end