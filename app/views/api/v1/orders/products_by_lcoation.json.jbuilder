json.status true
json.code 200
json.products do
   json.array! @products do |product|
   	 	json.id product.id
   	 	json.name product.name
			json.category product.category
			json.image1 product.image1
			json.image2 product.image2
			json.description product.description
			
			json.price product.price
			json.status product.status

			json.locations product.areas


   end
end