json.status true
json.code 200
json.user do
  json.partial! "user", user: @user
end