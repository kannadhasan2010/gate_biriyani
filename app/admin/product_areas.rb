ActiveAdmin.register ProductArea do
	menu priority: 5
	permit_params :product_id, :area_id, :status

	batch_action :update_location_to_product, form: -> { {area: Area.all.pluck(:name, :id) } } do |ids, inputs|
		ids.each do |id|
			product_area = ProductArea.find(id)
			product_area.area_id = inputs[:area]
			product_area.save
		end
		redirect_to collection_path, notice: "Products Location updated successfully"
	end 

	batch_action :update_product_to_location, form: -> { {product: Product.all.pluck(:name, :id) } } do |ids, inputs|
		ids.each do |id|
			product_area = ProductArea.find(id)
			product_area.product_id = inputs[:product]
			product_area.save
		end
		redirect_to collection_path, notice: "Products Location updated successfully"
	end 


	show do 
		attributes_table do
			row :id
			row :product
			row :area
			row :status
		end
	end

	index do
		selectable_column
		column :id
		column :product
		column :area
		column :status

		actions
	end


	form do |f|
		f.inputs do
			f.input :product
			f.input :area
		end
		f.actions
	end
end
