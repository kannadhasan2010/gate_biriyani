ActiveAdmin.register Product do
	menu priority: 4
	permit_params :category_id, :name, :description, :price, :status, :image1, :image2, :area
	filter :category
	filter :status
	actions :all, :except => [ :destroy]
	batch_action :update_product_category, form: -> { {category: Category.all.pluck(:name, :id) } } do |ids, inputs|
		ids.each do |id|
			product = Product.find(id)
			product.category_id = inputs[:category]
			product.save
		end
		redirect_to collection_path, notice: "Products Category updated successfully"
	end

	controller do
		def scoped_collection
			Product.where.not(status: :deleted)
		end
	end

	controller do
		def create
			product = Product.new
			product.category_id = params[:product]["category_id"]
			product.name = params[:product]["name"]
			product.description = params[:product]["description"]
			product.price = params[:product]["price"]
			product.image1 = params[:product]["image1"]
			product.image2 = params[:product]["image2"]
			if product.save
				params[:product]["area"].each do |id|
					product_area = ProductArea.new
					product_area.area_id = id
					product_area.product_id = product.id
					product_area.save
				end
			end
			redirect_to admin_products_path, notice: "Product added successfully"
		end

		def update

			product = Product.find(params[:id])
			product.category_id = params[:product]["category_id"]
			product.name = params[:product]["name"]
			product.description = params[:product]["description"]
			product.price = params[:product]["price"]
			product.image1 = params[:product]["image1"]
			product.image2 = params[:product]["image2"]
			product.status = params[:product]["status"]
			if product.save
				if params[:product]["area"].count > 1
					product.product_areas.delete_all
				  params[:product]["area"].each do |id|
						product_area = ProductArea.new
						product_area.area_id = id
						product_area.product_id = product.id
						product_area.save
					end
				end
			end
			redirect_to admin_products_path, notice: "Product updated successfully"
			
		end
	end


	show do 
		attributes_table do
			row :id
			row :category
			row :name
			row "Locations" do |product|
			all_labels = ''
        product.areas.each { |area|
          all_labels += ' '+ area.id.to_s + ":" + area.name
        }
       all_labels
		end
			row "image 1" do |product|
				raw "<img src='#{product.image1}' alt='#{product.name}' width='200' height='200'/> "
			end
			row "image 2" do |product|
				raw "<img src='#{product.image2}' alt='#{product.name}' width='200' height='200'/> "
			end
			row :description
			row :price
			row :status
		end
	end

	show do 
		panel "Product Details" do
			table_for product do
				column :id
			column :category
			column :name
			column "Locations" do |product|
			all_labels = ''
        product.areas.each { |area|
          all_labels += ' '+ area.id.to_s + ":" + area.name
        }
       all_labels
		end
			column "image 1" do |product|
				raw "<img src='#{product.image1}' alt='#{product.name}' width='200' height='200'/> "
			end
			column "image 2" do |product|
				raw "<img src='#{product.image2}' alt='#{product.name}' width='200' height='200'/> "
			end
			column :description
			column :price
			column :status
			end
		end

		panel " Product Locations" do
			table_for product.areas do
				column :id
				column :name
				column :code
				column :status
			end
		end
	end

	index do
		selectable_column
		column :id
		column :category
		column :name
		column "Locations" do |product|
			all_labels = ''
        product.areas.each { |area|
          all_labels += ' '+ area.id.to_s + ":" + area.name
        }
        all_labels
		end
		column "image 1" do |product|
			raw "<img src='#{product.image1}' alt='#{product.name}' width='200' height='200'/> "
		end
		column "image 2" do |product|
			raw "<img src='#{product.image2}' alt='#{product.name}' width='200' height='200'/> "
		end
		column :description
		column :price
		column :status
		actions
	end

	form do |f|
		f.inputs do
			f.input :category
			f.input :name
			f.input :description
			f.input :price
			f.input :image1
			f.input :image2
			f.input :area, as: :check_boxes, collection: Area.all
			f.input :status

		end
		f.actions
	end
end
