ActiveAdmin.register AdminUser do
  menu priority: 1

  permit_params :email, :password, :password_confirmation, :scope, :firstname , :lastname, :avatar,:status

  SCOPE = [["Admin", "admin"],["Employee", "employee"]]
  actions :all, :except => [:destroy]
  index do
    selectable_column
    id_column
    column :firstname
    column :lastname
    column :email
    column :scope
    column :status
    actions
  end

  show do |ad|
    attributes_table do
      row :id
      row :firstname
      row :lastname
      row :email
      row :scope
      row :status
    end
  end

  filter :false
  form do |f|
    f.inputs do
      f.input :firstname
      f.input :lastname
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :scope, :as => :select, :collection => SCOPE
      f.input :avatar, as: :file
      f.input :status
    end
    f.actions
  end
end