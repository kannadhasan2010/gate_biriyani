ActiveAdmin.register CustomerAddress do
	permit_params :customer_id, :name, :address1, :address2, :street, :city, :landmark, :pincode, :phone
	actions :all, :except => [:destroy]
end
