ActiveAdmin.register Order do
	menu priority: 6
	permit_params  :customer_id, :customer_address_id, :status

	filter :customer
	filter :product
	filter :customer_address
	filter :status

	batch_action :order_delivery_employee_for, form: -> { { employee: AdminUser.where(scope: "employee").pluck(:firstname, :id) } } do |ids, inputs|
		ids.each do |id|
			employee_order = EmployeeOrder.new
			employee_order.order_id = id
			employee_order.admin_user_id = inputs[:employee]
			employee_order.save
		end
		redirect_to collection_path, notice: "Order assigned for employee"
	end
	OrderStatus = [["placed", 0], ["packing",1], ["dispatching", 2], ["delivered",3], ["returned",4], ["canceled_by_user",5], ["canceled_by_admin",6]]
	batch_action :update_order_status, form: -> { { status: OrderStatus } } do |ids, inputs|
		ids.each do |id|
			order = Order.find(id)
			order.status = inputs[:status].to_i
			order.save
		end
		redirect_to collection_path, notice: "Order assigned for employee"
	end


	index do
		selectable_column
		column :id
		column :customer
		column :customer_address
		column :status
		column "Employee" do |order|
			order.admin_user
		end
		column "Total Amount" do |order|
			order.order_products.sum(:amount)
		end
		actions
	end

	show do 
		panel "Order Details" do
			table_for order do
				column :id
				column :customer
				column :customer_address
				column :status
				column "Employee" do |order|
					order.admin_user
				end
				column "Total Amount" do |order|
					order.order_products.sum(:amount)
				end
			end
		end

		panel "Order Products" do
			table_for order.order_products do
				column :product
				column :quantity
				column :amount
				column :description
			end
		end
	end



end
