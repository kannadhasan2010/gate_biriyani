ActiveAdmin.register Category do
	menu priority: 2
	permit_params :name, :description, :status
	menu :if => proc{ current_admin_user.scope==="admin" && current_admin_user.status==="active"}
	
	form do |f|
		f.inputs do
			f.input :name
			f.input :description
			f.input :status
		end
		f.actions
	end
	
end
