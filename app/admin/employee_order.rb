ActiveAdmin.register EmployeeOrder do
	permit_params :admin_user_id, :order_id, :description, :status
	filter :admin_user
	filter :order
	filter :description
	filter :status
	EmployeeOrderStatus = [["assinged", 0], ["packing",1], ["dispatching", 2], ["delivered",3], ["returned",4], ["canceled_by_user",5], ["canceled_by_admin",6]]
	batch_action :update_order_status, form: -> { { status: EmployeeOrderStatus } } do |ids, inputs|
		ids.each do |id|
			emp_order = EmployeeOrder.find(id)
			emp_order.status = inputs[:status].to_i
			emp_order.save
			order = emp_order.order
			order.status = 	emp_order.status
			order.save
		end
		redirect_to collection_path, notice: "Order assigned for employee"
	end

	form do |f|
		f.inputs do
			f.input :admin_user, :as => :select, :collection => AdminUser.where(scope: "employee").pluck(:firstname, :id) 
			f.input :order, :as => :select, :collection => Order.all.pluck(:id, :id) 
			f.input :description
			f.input :status
			
		end
		f.actions
	end
end
