ActiveAdmin.register Customer do
	permit_params :name, :email, :phone, :email_verified, :phone_verified, :image, :status 
	actions :all, :except => [ :destroy]
	filter :email
	index do
		column :id
		column :name 
		column :email 
		column :phone
		column :image 
		column :status 
		actions
	end
	show do |ad|
		attributes_table do
			row :id
			row :name 
			row :email 
			row :phone
			row :image 
			row :status 
		end
	end
form do |f|
		f.inputs do
			f.input :name
			f.input :email
			f.input :password
			f.input :phone
			f.input :image, :as => :file

		end
		f.actions
	end


end
