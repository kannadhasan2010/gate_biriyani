ActiveAdmin.register Area do
	menu priority: 3
	permit_params :name, :code, :status
	menu :if => proc{ current_admin_user.scope==="admin" && current_admin_user.status==="active"}
	
	form do |f|
		f.inputs do
			f.input :name
			f.input :code
			f.input :status
		end
		f.actions
	end

end
