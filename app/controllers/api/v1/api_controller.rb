class Api::V1::ApiController < ApplicationController
	respond_to :json
	include ActionController::RequestForgeryProtection
	include ActionController::HttpAuthentication::Token::ControllerMethods
	protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format =~ %r{application/json} }
	rescue_from ActiveRecord::RecordNotFound, with: :not_found

	protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format =~ %r{application/json} }
	rescue_from ActiveRecord::RecordNotFound, with: :not_found

	private

	def authenticated_user
		authenticate_or_request_with_http_token do |token, options|
			@current_user = ::Customer.find_by_auth_token(token)
		end
		unless @current_user
			return head(:unauthorized)
		end
	end
end
