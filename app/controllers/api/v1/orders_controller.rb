class Api::V1::OrdersController <  Api::V1::ApiController
	before_action :authenticated_user
	def new_order
		begin
			address_id = ""
			if !params[:address_id].blank?
				address_id = params[:address_id]
			else

				if !params[:shipping_address].blank?
					customer_address = CustomerAddress.find_by(name: params[:shipping_address][:name])
					if customer_address.blank?
						customer_address = CustomerAddress.new
						customer_address.customer_id = @current_user.id
						customer_address.name = params[:shipping_address][:name]
						customer_address.address1 = params[:shipping_address][:address1]
						customer_address.address2 = params[:shipping_address][:address2]
						customer_address.street = params[:shipping_address][:street]
						customer_address.city = params[:shipping_address][:city]
						customer_address.landmark = params[:shipping_address][:landmark]
						customer_address.pincode = params[:shipping_address][:pincode]
						customer_address.phone = params[:shipping_address][:phone]
						customer_address.save
						address_id = customer_address.id
					else
						address_id = customer_address.id
					end
				else
					render json: { status: false, code: 401 ,  message: 'Shipping address should not be empty'}, status: :bad_request
				end

			end
			if address_id.blank?
				render json: { status: false, code: 401 ,  message: 'Shipping address should not be empty'}, status: :bad_request
			else
				@order = Order.new
				@order.customer_id = @current_user.id
				@order.customer_address_id = address_id
				if @order.save
					products = params[:orders]
					products.each do |product|
						@product = Product.find(product[:product_id])
						@order_product  = OrderProduct.new
						@order_product.product_id = @product.id
						@order_product.order_id = @order.id
						@order_product.quantity = product[:quantity]
						@order_product.description = product[:description]
						@order_product.amount = (@order_product.quantity*@product.price)
						@order_product.save
					end
					render json: { status: true, code: 200, message: 'Your order placed successfully' }
				end
			end
		rescue Exception => e
			render json: { status: false, code: 401 ,  message: 'Unable to process your order',  error: e.message}, status: :bad_request
		end

	end

	def order_list
		@orders =  @current_user.orders
		if @orders.blank?
			render json: { status: false, code: 404, message: 'Record not found' }, status: 404 and return
		end
	end

	def cancel_order_by_customer
		begin
			@order  = Order.find_by(user_id: @current_user, id: params[:order_id])
			unless @orders
				render json: { status: false, code: 404, message: 'Record not found' }, status: 404 and return
			end
			@order.status = :canceled_by_user
			if @order.save
				render json: { status: true, code: 200, message: 'Your order has been cancelled' }
			end

		rescue Exception => e
			render json: { status: false, code: 401 ,  message: 'Unable to process your request',  error: e.message}, status: :bad_request
		end
	end

	def show_my_order
		begin
			@order  = Order.find_by(user_id: @current_user, id: params[:order_id])
			unless @order
				render json: { status: false, code: 404, message: 'Record not found' }, status: 404 and return
			end
		rescue Exception => e
			render json: { status: false, code: 401 ,  message: 'Unable to process your request',  error: e.message}, status: :bad_request
		end
	end

	def products_by_lcoation
		begin
			@location = Area.find_by(code: params[:code], status: :active)
			if @location.blank?
				render json: { status: false, code: 404, message: "Location Not found"  },status: :bad_request 
			else
				@products = @location.products.where(status: :active)
				unless @products
					render json: { status: false, code: 404, message: 'Record not found' }, status: 404 and return
				end
			end

		rescue Exception => e
			render json: { status: false, code: 400 ,  message: 'Unable to process your order',  error: e.message}, status: :bad_request
		end
	end
end

