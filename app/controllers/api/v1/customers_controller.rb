class Api::V1::CustomersController <  Api::V1::ApiController
	before_action :authenticated_user

	def profile
		@user =  @current_user
	end

	def customer_addresses
		@customer_addresses = @current_user.customer_addresses
		if @customer_addresses.blank?
			render json: { status: false, code: 404, message: 'Record not found' }, status: 404 and return
		else
			render json: { status: true, code: 200, customer_addresses: @customer_addresses }
		end
	end

	def verify_email
		begin
			@user = Customer.find_by(email: params[:email])
			unless @user
				render json: { status: false, code: 404, message: 'Record not found', errors: ["No account exists with this email."] }, status: 404 and return
			end
			if params[:token].present? && !params[:token].blank?
				@user.email_verified = true	if @user.email_verification_token==params[:token]
			end
			if @user.save!
				 update_verification_pending(@user)
			end
		rescue Exception => e
			render json: { status: false, code: 401, message: "Customer Email Verification failed", erors: e.message }, status: :bad_request 
		end
	end

	def update
		begin
			@user = @current_user
			if @user.update(update_params)	
				render json: { status: true, code: 200, message: "Profile updated"  }
			end
		
		rescue Exception => e
			render json: { status: false, code: 401, message: "Unable to update your profile", erors: e.message }, status: :bad_request 
		end
		
	end

	def verify_phone
		begin
			@user = Customer.find_by(phone: params[:phone])
			unless @user
				render json: { status: false, code: 404, message: 'Record not found', errors: ["No account exists with this phone."] }, status: 404 and return
			end
			if params[:token].present? && !params[:token].blank?
				@user.phone_verified = true	if @user.phone_verification_token==params[:token]
			end
			if @user.save!
					@user = update_verification_pending(@user)
			end
		rescue Exception => e
			render json: { status: false, code: 401, message: "Customer Phone Verification failed", erors: e.message }, status: :bad_request 
		end
	end

	def update_verification_pending(user)
		if user.email_verified==true
			if user.email_verified==true && user.phone_verified==true
				user.verification_pending_for = :nothing
				user.status = :active
				user.save
				return user
			else
				user.verification_pending_for = :phone
				user.save
				return user
			end

		elsif user.phone_verified==true
				if user.email_verified==true && user.phone_verified==true
				user.verification_pending_for = :nothing
				user.status = :active
				user.save
				return user
			else
				user.verification_pending_for = :email
				user.save
				return user
			end
		end	
	end

	private

	def update_params
		params.require(:customer).permit( :user_name,  :image )
	end
	
	
end
