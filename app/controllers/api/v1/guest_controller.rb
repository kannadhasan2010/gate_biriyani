class Api::V1::GuestController < Api::V1::ApiController

	def sign_up
		begin
			render json: { status: false, message: 'Email already Registered', errors: ["Account exists with this email."] } and return if Customer.exists?(email: params[:user][:email])
			@user = Customer.new(create_params)
			@user.save
			#UserMailer.welcome_email(@user).deliver_now
		rescue Exception => e
			render json: { status: false, code: 401, message: 'Unable to sign up',  error: e.message}, status: :bad_request
		end
	end

	def sign_in
		validate = login_details_blank?
		render json: validate, status: :bad_request and return if !validate[:status]
		@user = ::Customer.find_by(email: params[:email], status: :active) rescue nil
		unless @user
			render json: { status: false, code: 404, message: 'Record not found', errors: ["No account exists with this email."] }, status: 404 and return
		end
		if authenticate(@user)
			@user.auth_token = update_auth_token(@user.email)
			@user.save
		else
			render json: { status: false, code: 401, message: 'Password does not match.',  errors: { password: ["Your entered incorrect password"] } }, status: :bad_request and return
		end
	end

	def password_reset_request
		begin
			@user = Customer.find_by(email: params[:email])
			unless @user
				render json: { status: false,code: 404, message: 'Record not found', errors: ["No account exists with this email."] }, status: 404 and return
			end
			@user.reset_token = generate_password_reset_token(@user.email)
			@user.reset_token_expiry = DateTime.now + 10.minute
			if @user.save!
				render json: { status: true, code: 200, password_reset_token: @user.reset_token, message: "Your password reset link has been sent to your email" }
			end
		rescue Exception => e
			render json: { status: false, code: 401, message: "Your password reset request could not processed", erors: e.message }, status: :bad_request 
		end
	end
	
	def update_password
	
		if params[:password].blank?
			render json: { status: false, code: 401, message: "Old password should not be empty", validation_for: ["password"]  }, status: :bad_request 
		else
			@user = Customer.find_by(reset_token: params[:password_reset_token])
			current_datetime = DateTime.now
			
			if current_datetime > @user.reset_token_expiry
				render json: { status: false, code: 401, message: "Password reset time expired", validation_for: ["password_reset_token"]    }, status: :bad_request 
			elsif  @user.blank?
				render json: { status: false, code: 401, message: "User not found with current password"  }, status: :bad_request 
			else
				if params[:new_password].blank?
					render json: { status: false,code: 401, message: "New password should not be empty", validation_for: ["new_password"] }, status: :bad_request 
				else
					if authenticate(@user)
						encrypted_password = PasswordEncryptor.new(params[:new_password]).salted
						@user.salt = encrypted_password.salt
						@user.password = encrypted_password.hash
						if @user.save
							render json: { status: true, code: 200, message: "Your account password has been changed successfully"  }
						else
							render json: { status: false, code: 401, message: "Your account password could not reset. try again"  }, status: :bad_request 
						end
					else
						render json: { status: false, code: 401, message: "Your entered incorrect old password", validation_for: ["password"] }, status: :bad_request 
					end
				end
			end
		end
	end


	def verify_email
		begin
			@user = Customer.exists?(email: params[:email], email_verified: true)
			flash[:notice] = "Your email already verified" and return if @user
			@user = Customer.find_by(email: params[:email])
			unless @user
				flash[:notice] = "No account exists with this email." and return
			end
			if params[:token].present? && !params[:token].blank?
				
			end
			if @user.email_verification_token==params[:token]
				@user.email_verified = true	
				@user.save!
				 update_verification_pending(@user)
				 flash[:notice] = "Your Email has been verified.... Thank you for joining us"
			else
					flash[:notice] = "Email Verification token is invalid" 
			end
			
		rescue Exception => e
			flash[:notice] = "Unable to verify your email.. Sorry!!!"
		end
	end

	def update_verification_pending(user)
		if user.email_verified==true
			if user.email_verified==true && user.phone_verified==true
				user.verification_pending_for = :nothing
				user.status = :active
				user.save
				return user
			else
				user.verification_pending_for = :phone
				user.save
				return user
			end

		elsif user.phone_verified==true
				if user.email_verified==true && user.phone_verified==true
				user.verification_pending_for = :nothing
				user.status = :active
				user.save
				return user
			else
				user.verification_pending_for = :email
				user.save
				return user
			end
		end	
	end
	

	private
	
	def login_details_blank?
		if params[:email].blank? && params[:password].blank?
			return { status: false, code: 401, message: "Validation errors in your request", errors: { email: ["Email field is empty"], password: ["Password field is empty"]  } }, status: :bad_request
		elsif params[:email].blank?
			return { status: false, code: 401, message: "Validation errors in your request", errors: { email: ["Email field is empty"] } }, status: :bad_request
		elsif params[:password].blank?
			return { status: false, code: 401, message: "Validation errors in your request", errors: { password: ["Password field is empty"] } }, status: :bad_request
		else
			return { status: true }
		end
	end

	def update_auth_token(email)
		loop do
			token = SecureRandom.base64.tr('+/=', email).delete('\/')
			break token unless Customer.exists?(auth_token: token)
		end
	end
	
	def generate_password_reset_token(email)
		loop do
			token = EmailVerificationTokenGenerator.new(email).generate_token.delete('\/')
			break token unless Customer.exists?(reset_token: token)
		end
	end

	def authenticate(user)
		return false unless user
		user.password == PasswordEncryptor.new(params[:password], :salt => user.salt).salted.hash
	end

	def create_params
		params.require(:user).permit( :name, :email, :password, :phone, :image)
	end

end
