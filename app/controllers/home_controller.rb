class HomeController < ApplicationController
 before_action :authenticate_admin_user!
  def index
  	if current_admin_user
  		redirect_to admin_root_path
  	end
  end
end
