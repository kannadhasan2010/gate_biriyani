class Category < ApplicationRecord
	has_many :products
	enum status: { inactive: 0, active: 1, deleted:2 }
	after_save :assign_product_category_status
	def assign_product_category_status
		products = self.products
		products.each do |product|
			product.status = self.status
			product.save
		end
	end
end
