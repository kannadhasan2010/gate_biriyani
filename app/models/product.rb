class Product < ApplicationRecord
  belongs_to :category
  has_many :orders
  has_many :product_areas
	has_many :areas , through: :product_areas
  enum status: { inactive: 0, active: 1, deleted:2 }
  # mount_uploader :image1, ProductImageUploader
  # mount_uploader :image2, ProductImageUploader
end
