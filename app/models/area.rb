class Area < ApplicationRecord
	has_many :product_areas
	has_many :products , through: :product_areas
	enum status: { inactive: 0, active: 1, deleted:2 }
end
