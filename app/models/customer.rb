class Customer < ApplicationRecord
	mount_base64_uploader :image, UserAvatarUploader

	enum verification_pending_for: { email_and_phone: 0, phone: 1, email:2, nothing: 3 }
	enum status: { inactive: 0, active: 1, deleted:2 }
	before_create :assign_salt_and_hashed_password
	before_create :assign_auth_token
	before_create :assign_email_verification_token
	before_create :assign_phone_otp

	has_many :orders
	has_many :customer_addresses

	def assign_auth_token
		self.auth_token = generate_auth_token
		self.status = :active 
	end 

	def assign_salt_and_hashed_password
		encrypted_password = PasswordEncryptor.new(self.password).salted
		self.salt = encrypted_password.salt
		self.password = encrypted_password.hash
	end

	def assign_email_verification_token
		self.email_verification_token = generate_email_verification_token
	end

	def assign_phone_otp
		self.phone_verification_token = PhoneVerificationTokenGenerator.new(self.phone).generate_token
	end

	def generate_email_verification_token
		loop do
			token = EmailVerificationTokenGenerator.new(self.email).generate_token.delete('\/')
			break token unless Customer.exists?(email_verification_token: token)
		end
	end

	def generate_auth_token
		loop do
			token = SecureRandom.base64.tr('+/=', self.email).delete('\/')
			break token unless Customer.exists?(auth_token: token)
		end
	end
end
