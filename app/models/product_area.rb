class ProductArea < ApplicationRecord
  belongs_to :product
  belongs_to :area
  enum status: { inactive: 0, active: 1, deleted:2 }
end
