class Order < ApplicationRecord
	belongs_to :customer
	belongs_to :customer_address
	has_one :employee_order
	has_one :admin_user, :through => :employee_order
	enum status: { placed: 0, packing:1, dispatching: 2, delivered:3, returned:4, canceled_by_user:5, canceled_by_admin:6 }
	has_many :order_products
end
