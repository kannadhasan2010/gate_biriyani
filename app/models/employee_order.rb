class EmployeeOrder < ApplicationRecord
  belongs_to :admin_user
  belongs_to :order
  has_one :employee_order
  enum status: { assigned: 0, packing:1, dispatching: 2, delivered:3, returned:4, canceled_by_user:5, canceled_by_admin:6 }
end
