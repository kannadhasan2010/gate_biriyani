Rails.application.routes.draw do
  root 'home#index'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	get "guest/customer/verify_email" => "api/v1/guest#verify_email"
  namespace :api do
		namespace :v1 do
			post "sign_up" => "guest#sign_up"
			
			post "sign_in" => "guest#sign_in"
			
			get "forgot_password" => "guest#password_reset_request"
			
			put "update_password" => "guest#update_password"

			# get "guest/customer/verify_email" => "guest#verify_email"
		
			get "customer/profile" => "customers#profile"
			put "customer/profile" => "customers#update"

			get "customer/customer_addresses" => "customers#customer_addresses" 



			post "customer/verify_email" => "customers#verify_email"

			post "customer/verify_phone" => "customers#verify_phone"

			get "location_poroducts" => "orders#products_by_lcoation"

			get "customer/orders" => "orders#order_list"

			post "customer/order/new" => "orders#new_order"

			put  "customer/order/cancel/:order_id" => "orders#cancel_order_by_customer"

			post "customer/order/show/:order_id" => "orders#show_my_order"
		end
	end
end
